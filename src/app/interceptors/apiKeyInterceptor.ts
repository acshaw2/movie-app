import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class ApiKeyInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
    // #Blocked-scope variables let and const
    // Use const to denote a variable that is scoped to the block it is defined
    // and that it will not change. Let is the same thing except it is not constant

    const keyedRequest = req.clone({
      url: req.url.concat('&api_key=a1917d65cfd6c533a238ed33ccf80353')
    });

    return next.handle(keyedRequest);
  }
}