import {Component, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/search/services/search.service';
import {  MovieCollectionService } from '../../services/movie-collection.service';

@Component({
    selector: 'dashboard-movie-favorites',
    templateUrl: './dashboard-movie-favorites.component.html',
    styleUrls: ['./dashboard-movie-favorites.component.css']
})

export class DashboardMovieFavoritesComponent { 
    public loading = false;
    public displayedColumns: string[] = ['title', 'actions0', 'actions1'];
    public dataSource: MatTableDataSource<any>;  
    public favoriteMovies: any[] = [];

    @ViewChild(MatSort) sort: MatSort = new MatSort();
    
    constructor(private _router: Router, public _collectionService: MovieCollectionService, private _searchService: SearchService){ 
        this.dataSource = new MatTableDataSource(this._collectionService.favoriteMovies);
        this.favoriteMovies = this._collectionService.favoriteMovies;
        this._searchService.UpdateRoute({});
        this._searchService.ClearSearchData();
    }
    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
    }
    
    public clickFavoriteToggle(event: boolean, movie: any){
        if (event as unknown as boolean === true)
            this._collectionService.AddMovie(movie);
        if (event as unknown as boolean === false)    
            this.favoriteMovies = this._collectionService.RemoveMovie  (movie); 
    }
    
    public isFavorite(movie: any) : boolean
    {
        let isFavorite = false;
        if (this.favoriteMovies.filter(favoriteMovies => favoriteMovies.id === movie.id).length >= 1) {
            isFavorite = true;
        }    
        return isFavorite;
    }
    
    public getMovieDetails(row:any){        
        this._searchService._route.queryParams.subscribe(params => {
            this._router.navigate([], {
                relativeTo: this._searchService._route,
                queryParams: { },
                queryParamsHandling: 'merge',
                // preserve the existing query params in the route
                skipLocationChange: true
                // do not trigger navigation
            });          
        });  

        this._router.navigateByUrl(`/details/movie/${row.id}`);
    } 
}
