import {Component} from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/search/services/search.service';
import {  MovieCollectionService } from '../../services/movie-collection.service';

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent { 
    public loading = false;
    displayedColumns: string[] = ['name', 'actions0', 'actions1'];
    dataSource: any[] = [];  
    public favoriteMovies: any[] = [];
    public favoritePeople: any[] = [];

    constructor(private _router: Router, public _collectionService: MovieCollectionService, private _searchService: SearchService){ 
        this.favoriteMovies = this._collectionService.favoriteMovies;
        this.favoritePeople = this._collectionService.favoritePeople;
        this._searchService.UpdateRoute({});
        this._searchService.ClearSearchData();
    }

    public RedirectToSearch(){
        this._router.navigateByUrl('/moviesearch');
    }

    public RedirectToPeopleSearch(){
        this._router.navigateByUrl('/peoplesearch');
    } 
}