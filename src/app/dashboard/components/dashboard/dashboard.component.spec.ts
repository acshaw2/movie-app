import { of } from "rxjs";
import { SearchService } from "src/app/search/services/search.service";
import { MovieCollectionService } from "../../services/movie-collection.service";
import { DashboardComponent } from "./dashboard.component"

describe('DashboardComponent', () => {
    let FAVORITEMOVIES: { id: number; title: string; release_date: number; }[];
    let UPDATEDMOVIES: { id: number; title: string; release_date: number; }[];
    let component;
    let mockRouter;
    let mockCollectionService;
    let mockSearchService;


    // beforeEach(() => {
    // });
    
    describe('RemoveItem', () => {
        it('should remove a movie from favoriteMovies', () => {
            // arrange
            FAVORITEMOVIES = [
                {id: 1, title: 'Test Movie 1', release_date: 1980 },
                {id: 2, title: 'Test Movie 2', release_date: 1985 },
                {id: 3, title: 'Test Movie 3', release_date: 1990 }
            ];
            UPDATEDMOVIES = [
                {id: 1, title: 'Test Movie 1', release_date: 1980 },
                {id: 2, title: 'Test Movie 2', release_date: 1985 }
            ]    
            mockRouter = jasmine.createSpyObj(['navigateByUrl']);
            mockCollectionService = jasmine.createSpyObj(['favoriteMovies', 'GetFavoriteMovies', 'AddMovie', 'RemoveMovie']);
            mockSearchService = jasmine.createSpyObj(['searchData', 'searchDataFetched', 'params', 'updatedParams', 'UpdateRoute', 'ClearSearchData', 'GetMovies', 'GetPeople', 'createSearchRoute', 'getMovies', 'getPeople']);
            component = new DashboardComponent(mockRouter, mockCollectionService, mockSearchService);

            mockCollectionService.favoriteMovies = FAVORITEMOVIES;
            mockCollectionService.RemoveMovie.and.returnValue(UPDATEDMOVIES);
            
            component.favoriteMovies = FAVORITEMOVIES;

            // act
            component.RemoveMovie({id: 3, title: 'Test Movie 3', release_date: 1990 });

            // assert
            expect(component.favoriteMovies.length).toBe(2);
        });
    });
});

