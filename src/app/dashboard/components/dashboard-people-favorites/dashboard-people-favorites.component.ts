import {Component, ViewChild} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/search/services/search.service';
import {  MovieCollectionService } from '../../services/movie-collection.service';


@Component({
    selector: 'dashboard-people-favorites',
    templateUrl: './dashboard-people-favorites.component.html',
    styleUrls: ['./dashboard-people-favorites.component.css']
})

export class DashboardPeopleFavoritesComponent { 
    public loading = false;
    public displayedColumns: string[] = ['name', 'actions0', 'actions1'];
    public dataSource: MatTableDataSource<any>; 
    public favoritePeople: any[] = [];
    
    @ViewChild(MatSort) sort: MatSort = new MatSort();

    constructor(private _router: Router, public _collectionService: MovieCollectionService, private _searchService: SearchService){ 
        this.dataSource = new MatTableDataSource(this._collectionService.favoritePeople);
        this.favoritePeople = this._collectionService.favoritePeople;
        this._searchService.UpdateRoute({});
        this._searchService.ClearSearchData();
    }
    
    // #Lifecycle Hook
    // This particular hook is fired after Angular has fully initialized the component's view
    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
    
    }

    public clickFavoriteToggle(event: boolean, person: any){
        if (event as unknown as boolean === true)
            this._collectionService.AddPerson(person);
        if (event as unknown as boolean === false)    
            this.favoritePeople = this._collectionService.RemovePerson(person); 
    }
    
    public isFavorite(person: any) : boolean
    {
        let isFavorite = false;
        if (this.favoritePeople.filter(favoritePeople => favoritePeople.id === person.id).length >= 1) {
            isFavorite = true;
        }    
        return isFavorite;
    }

    public getPersonDetails(row:any)
    {      
        this._searchService._route.queryParams.subscribe(params => {
            this._router.navigate([], {
                relativeTo: this._searchService._route,
                queryParams: { },
                queryParamsHandling: 'merge',
                // preserve the existing query params in the route
                skipLocationChange: true
                // do not trigger navigation
            });          
        });  
    
        this._router.navigateByUrl(`/details/person/${row.id}`);
    }  
}