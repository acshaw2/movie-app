// #this 
// Referes tot he scope of the component from which we are in

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MovieCollectionService {

  public favoriteMovies: any[] = [];
  public favoritePeople: any[] = [];

  constructor() { }

  public GetFavoriteMovies(){
    return this.favoriteMovies;
  }
  
  public GetFavoritePeople(){
    return this.favoritePeople;
  }

  public AddMovie(movie: any): void {
    this.favoriteMovies.push(movie);
  }  
  
  // #Array Function
  // Push is an array function that adds items to the array, pop, shift, unshift would be others
  public AddPerson(person: any): void {
    this.favoritePeople.push(person);
  }
  
  public RemoveMovie(movie: any): any {
    // #Filter
    // here is an example of using filter to remove one movie by id from the favorite movies
    this.favoriteMovies = this.favoriteMovies.filter(res => res.id != movie.id);
    return this.favoriteMovies;
  }

  public RemovePerson(person: any): any {
    this.favoritePeople = this.favoritePeople.filter(res => res.id != person.id);
    return this.favoritePeople;
  }
}

// export interface IMovie{
//   id: Number,
//   title: string
// }
