import { MovieCollectionService } from "./movie-collection.service";

// #Arrow function
// Tests make heavy use of arrow functions, which are basically short hand for
// anonymous functions with some extra limitations
describe('MovieCollectionService', () => {
    let _movieCollectionService: MovieCollectionService

    beforeEach(() => {
        // arrange for all tests here b/c they are such simple tests
        _movieCollectionService = new MovieCollectionService();
    });

    describe('Init', () => {
        it('should have no movies to start', () => {
            // arrange
            _movieCollectionService = new MovieCollectionService();
    
            // no act necessary
    
            // assert
            expect(_movieCollectionService.favoriteMovies.length).toBe(0);
        });
    })

    describe('AddMovie', () => {
        it('should add a movie when AddMovie is called', () => {
            // arrange
            _movieCollectionService = new MovieCollectionService();
            
            // act
            _movieCollectionService.AddMovie({id: 1, title: 'Test Movie', release_date: 1980 });

            // assert
            expect(_movieCollectionService.favoriteMovies.length).toBe(1);
        });
    });

    describe('RemoveMovie', () => {
        it('should remove a movie when RemoveMovie is called', () => {
            // arrange for all tests here b/c they are such simple tests
            _movieCollectionService = new MovieCollectionService();
            _movieCollectionService.AddMovie({id: 1, title: 'Test Movie', release_date: 1980 });
            
            // act
            _movieCollectionService.RemoveMovie({id: 1, title: 'Test Movie', release_date: 1980 });

            // assert
            expect(_movieCollectionService.favoriteMovies.length).toBe(0);
        });
    });
});