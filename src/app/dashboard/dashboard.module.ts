import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardGuardService } from './services/dashboard-guard.service';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MovieCollectionService } from './services/movie-collection.service';
import { SearchModule } from '../search/search.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DashboardPeopleFavoritesComponent } from './components/dashboard-people-favorites/dashboard-people-favorites.component';
import { DashboardMovieFavoritesComponent } from './components/dashboard-movie-favorites/dashboard-movie-favorites.component';
import { MatButtonModule } from '@angular/material/button';
import { MatSortModule } from '@angular/material/sort';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  declarations: [
    DashboardComponent,
    DashboardPeopleFavoritesComponent,
    DashboardMovieFavoritesComponent
  ],
  exports: [
    DashboardComponent,
    DashboardPeopleFavoritesComponent,
    DashboardMovieFavoritesComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatSortModule,
    MatProgressSpinnerModule,
    SearchModule,
    MatPaginatorModule,
    MatTableModule
  ],
  providers: [
    DashboardGuardService,
    MovieCollectionService
  ]
})
export class DashboardModule { }
