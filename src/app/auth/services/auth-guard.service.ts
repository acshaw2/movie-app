// #Guard
// This guard service prevents unAuthenticated users from reaching the dashboard page
// The guard is consumed inside the app-routing module
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    const isAuthenticated = this.auth.IsAuthenticated();

    if (isAuthenticated) {
      this.router.navigate(['dashboard']);
    }

    return !isAuthenticated;
  }
}