import { Injectable } from '@angular/core';
import { of, observable, Observable, Subject } from 'rxjs'; 
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isAuthenticated: boolean = false;
  isAuthenticatedChange: Subject<boolean> = new Subject<boolean>();

  constructor() { 
    this.isAuthenticatedChange.subscribe((value) => {
    this.isAuthenticated = value
});}

  public AuthenticateUser(_userName: string, _password: string): Observable<IUser>{
     this.toggleAuthenticated();
     var authenticatedUser = { userName: _userName, password: _password};
    return of<IUser>(authenticatedUser).pipe(delay(1500));
  }

  public IsAuthenticated(): boolean {    
    return this.isAuthenticated;
  }

  public toggleAuthenticated() {
    this.isAuthenticatedChange.next(!this.isAuthenticated);
  }
}

export interface IUser {
  userName: string;
  password: string;
}
