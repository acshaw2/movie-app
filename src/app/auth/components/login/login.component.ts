import {Component} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthService, IUser } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent { 
    public hide = true;
    public loginForm: FormGroup;
    public loading: boolean = false;

    // #Form
    // This component demonstrates an example of a Reactive Form
    constructor(private _fb: FormBuilder, private _router: Router, private _authService: AuthService) {
        this.loginForm = this._fb.group({
            // #Validators
            // Here are some examples of built-in validators
            userName: new FormControl('', [Validators.required, Validators.minLength(6) ]),
            password: new FormControl('', [Validators.required, Validators.minLength(6) ])
        });
    }

    public onSubmit(form: FormGroup): void{
        if (!this.loginForm.invalid) {
            this.loading = true;
            this._authService.AuthenticateUser(
                this.loginForm.get('password')?.value,
                this.loginForm.get('userName')?.value)
            .pipe()
            .subscribe((result: IUser) => { 
                this._router.navigateByUrl('/dashboard');
                this.loading = false;
            });
        }
    }

}