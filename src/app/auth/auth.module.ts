import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginComponent } from './components/login/login.component';
import { AuthService } from './services/auth.service';

import { MatCardModule } from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';

import {ReactiveFormsModule} from '@angular/forms';
import { AuthGuardService } from './services/auth-guard.service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { DashboardGuardService } from '../dashboard/services/dashboard-guard.service';



@NgModule({
  declarations: [
    LoginComponent
  ],
  exports: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthService,
    AuthGuardService,
    DashboardGuardService
  ]
})
export class AuthModule { }
