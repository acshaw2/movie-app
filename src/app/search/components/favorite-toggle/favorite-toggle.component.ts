// #Component
// Here is an example of a very simple reuesable component for favorite toggling
// It does not manage any application state itself, instead it only emits a switch
// which its parent compoent may handle 

import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'favorite-toggle',
    templateUrl: './favorite-toggle.component.html',
    styleUrls: ['./favorite-toggle.component.css']
})

export class FavoriteToggleComponent { 
    // #Input
    // This input is passed in from the parent component and defaults as false
    @Input() isFavorite: boolean = false;

    // #Output
    // This output emits an event when the component is clicked to notify its
    // parent component of the toggled favorite value
    @Output() toggleFavorite = new EventEmitter<boolean>();

    constructor(){ }

    public clickFavoriteToggle(){
        this.toggleFavorite.emit(!this.isFavorite);
    }

}