import {Component} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SearchService } from '../../services/search.service';

@Component({
    selector: 'people-search-filters',
    templateUrl: './people-search-filters.component.html',
    styleUrls: ['./people-search-filters.component.css']
})

export class PeopleSearchFiltersComponent { 
    public pageIndex = 0;
    public peopleSearchForm: FormGroup;
    public loading: boolean = false;

    constructor(private _fb: FormBuilder, private _router: Router, private _searchService: SearchService) {
        this.peopleSearchForm = this._fb.group({
            searchTerm: new FormControl('', [Validators.required, Validators.minLength(3) ])
        });

        if(this._searchService.params.query){
            this._searchService.GetPeople(
                this._searchService.params.query, 
                this._searchService.params.page);

            this.peopleSearchForm.get('searchTerm')?.setValue(this._searchService.params.query);
        }
    }

    public onSubmit(form: FormGroup): void{
        if(!form.invalid){
            this._searchService.GetPeople(
                this.peopleSearchForm.get('searchTerm')?.value, 
                this.pageIndex);

            this._searchService._route.queryParams.subscribe(params => {
                this._router.navigate([], {
                    relativeTo: this._searchService._route,
                    queryParams: {
                    query: this.peopleSearchForm.get('searchTerm')?.value,
                    page: this.pageIndex
                    },
                    queryParamsHandling: 'merge',
                    // preserve the existing query params in the route
                    skipLocationChange: true
                    // do not trigger navigation
                });          
            });              
        }      
    }
}