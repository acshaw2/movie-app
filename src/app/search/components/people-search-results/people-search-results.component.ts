import {Component} from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieCollectionService } from 'src/app/dashboard/services/movie-collection.service';
import { SearchService } from '../../services/search.service';

export interface Movie {
  original_title: string,
  adult: boolean,
  release_date: string
}

@Component({
    selector: 'people-search-results',
    templateUrl: './people-search-results.component.html',
    styleUrls: ['./people-search-results.component.css']
})

export class PeopleSearchResultsComponent { 
  displayedColumns: string[] = ['name', 'popularity', 'actions0', 'actions1'];
  dataSource: any[] = [];  
  public favoritePeople: any[] = [];

  constructor(public _searchService: SearchService, public _router: Router, public _route: ActivatedRoute, public _collectionService: MovieCollectionService){
    this.favoritePeople = this._collectionService.favoritePeople;
    if(this._searchService.params)
      this.dataSource = this._searchService.searchData;
  }

  public getPeople(event?:PageEvent) {
    this._searchService.GetPeople('back', event?.pageIndex ? event.pageIndex : 0);
    return event;
  }

  public getPersonDetails(row:any){        
    
    this._searchService._route.queryParams.subscribe(params => {
      this._router.navigate([], {
          relativeTo: this._searchService._route,
          queryParams: { },
          queryParamsHandling: 'merge',
          // preserve the existing query params in the route
          skipLocationChange: true
          // do not trigger navigation
      });          
    });  

    this._router.navigateByUrl(`/details/person/${row.id}`);
  }  

  public clickFavoriteToggle(event: boolean, person: any){
    if (event as unknown as boolean === true)
      this._collectionService.AddPerson(person);
    if (event as unknown as boolean === false)    
      this.favoritePeople = this._collectionService.RemovePerson(person); 
  }

  public isFavorite(person: any) : boolean{
    let isFavorite = false;
    if (this.favoritePeople.filter(favoritePeople => favoritePeople.id === person.id).length >= 1) {
      isFavorite = true;
    }    
    return isFavorite;
  }
    
}