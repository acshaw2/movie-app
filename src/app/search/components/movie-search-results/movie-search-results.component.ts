import { LEADING_TRIVIA_CHARS } from '@angular/compiler/src/render3/view/template';
import {Component} from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieCollectionService } from 'src/app/dashboard/services/movie-collection.service';
import { SearchService } from '../../services/search.service';

export interface Movie {
  original_title: string,
  adult: boolean,
  release_date: string
}

@Component({
    selector: 'movie-search-results',
    templateUrl: './movie-search-results.component.html',
    styleUrls: ['./movie-search-results.component.css']
})

export class MovieSearchResultsComponent { 
  displayedColumns: string[] = ['original_title', 'release_date', 'actions_1', 'actions'];
  dataSource: any[] = [];  
  public favoriteMovies: any[] = [];

  constructor(public _searchService: SearchService, public _router: Router, public _route: ActivatedRoute, private _collectionService: MovieCollectionService ){
    this.favoriteMovies = this._collectionService.favoriteMovies;
    if(this._searchService.params)
      this.dataSource = this._searchService.searchData;
  }

  public clickFavoriteToggle(event: boolean, movie: any){
    if (event as unknown as boolean === true)
      this._collectionService.AddMovie(movie);
    if (event as unknown as boolean === false)    
      this.favoriteMovies = this._collectionService.RemoveMovie(movie); 
  }

  public isFavorite(movie: any) : boolean{
    let isFavorite = false;
    if (this.favoriteMovies.filter(favoriteMovie => favoriteMovie.id === movie.id).length >= 1) {
      isFavorite = true;
    }    
    return isFavorite;
  }

  public getMovies(event?:PageEvent) {
    this._searchService.GetMovies('back', event?.pageIndex ? event.pageIndex : 0);
    return event;
  }

  public getMovieDetails(row:any){        
    
    this._searchService._route.queryParams.subscribe(params => {
      this._router.navigate([], {
          relativeTo: this._searchService._route,
          queryParams: { },
          queryParamsHandling: 'merge',
          // preserve the existing query params in the route
          skipLocationChange: true
          // do not trigger navigation
      });          
    });  

    this._router.navigateByUrl(`/details/movie/${row.id}`);
  }
    
}