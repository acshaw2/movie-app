import {Component, EventEmitter, Output} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SearchService } from '../../services/search.service';

@Component({
    selector: 'movie-search-filters',
    templateUrl: './movie-search-filters.component.html',
    styleUrls: ['./movie-search-filters.component.css']
})

export class MovieSearchFiltersComponent { 
    //@Output() public moviesFound = new EventEmitter<any[]>();
    public pageIndex = 0;
    public movieSearchForm: FormGroup;
    public loading: boolean = false;

    constructor(private _fb: FormBuilder, private _router: Router, private _searchService: SearchService) {
        this.movieSearchForm = this._fb.group({
            searchTerm: new FormControl('', [Validators.required, Validators.minLength(3) ]),
            searchYear: new FormControl('', [Validators.min(1900) ])
        });

        if(this._searchService.params.query){
            this._searchService.GetMovies(
                this._searchService.params.query, 
                this._searchService.params.page,
                this._searchService.params.year ? this._searchService.params.year : null);

            this.movieSearchForm.get('searchTerm')?.setValue(this._searchService.params.query);
            this.movieSearchForm.get('searchYear')?.setValue(this._searchService.params.year);
        }
    }

    public onSubmit(form: FormGroup): void{
        if(!form.invalid){
            this._searchService.GetMovies(
                this.movieSearchForm.get('searchTerm')?.value, 
                this.pageIndex,
                this.movieSearchForm.get('searchYear')?.value ? this.movieSearchForm.get('searchYear')?.value : null);

            this._searchService._route.queryParams.subscribe(params => {
                this._router.navigate([], {
                    relativeTo: this._searchService._route,
                    queryParams: {
                    query: this.movieSearchForm.get('searchTerm')?.value,
                    page: this.pageIndex,
                    year: this.movieSearchForm.get('searchYear')?.value ? this.movieSearchForm.get('searchYear')?.value : null
                    },
                    queryParamsHandling: 'merge',
                    // preserve the existing query params in the route
                    skipLocationChange: true
                    // do not trigger navigation
                });          
            });              
        }      
    }
}