// #Module
// Here is an example of using modules. This is the search module and 
// contains everything needed by the SearchModule components

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieSearchComponent } from './components/movie-search/movie-search.component';
import { MovieSearchFiltersComponent } from './components/movie-search-filters/movie-search-filters.component';
import { MovieSearchResultsComponent } from './components/movie-search-results/movie-search-results.component';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchService } from './services/search.service';
import { HttpClientModule } from '@angular/common/http';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import { PeopleSearchComponent } from './components/people-search/people-search.component';
import { PeopleSearchFiltersComponent } from './components/people-search-filters/people-search-filters.component';
import { PeopleSearchResultsComponent } from './components/people-search-results/people-search-results.component';
import { MatIconModule } from '@angular/material/icon';
import { FavoriteToggleComponent } from './components/favorite-toggle/favorite-toggle.component';

@NgModule({
  declarations: [
    MovieSearchComponent,
    MovieSearchFiltersComponent,
    MovieSearchResultsComponent,
    PeopleSearchComponent,
    PeopleSearchFiltersComponent,
    PeopleSearchResultsComponent,
    FavoriteToggleComponent
  ],
  exports: [
    MovieSearchComponent,
    MovieSearchFiltersComponent,
    MovieSearchResultsComponent,
    PeopleSearchComponent,
    PeopleSearchFiltersComponent,
    PeopleSearchResultsComponent,
    FavoriteToggleComponent
  ],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    HttpClientModule
  ],
  providers: [
    SearchService
  ]
})
export class SearchModule { }
