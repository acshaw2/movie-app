// #Service
// This is an example of a service that other components may use to draw centralized data from
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs'; 

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  public searchData: any = {results: []};
  public searchDataFetched: Subject<any> = new Subject<any>();

  public params!: Params;
  public updateParams: Subject<Params> = new Subject<Params>();

  constructor(private _http: HttpClient, private _router: Router, public _route: ActivatedRoute) {
    this.searchDataFetched.subscribe((value) => {
      this.searchData = value });

    this.updateParams.subscribe((value) => {
      this.params = value;
    });

    this._route.queryParams.subscribe(params => {
      this.updateParams.next(params);
    });
  }

  public UpdateRoute(newParams: any){
    this.updateParams.next(newParams);
  }

  public ClearSearchData(){
    this.searchDataFetched.next({results: []});
  }

  public GetMovies(query:string = '', page:number = 0,  year?:number): void {
    this.getMovies(query, page, year)
      .pipe()
      .subscribe((result: any) => {         
          this.searchDataFetched.next(result);
      });
  }

  public GetPeople(query:string = '', page:number = 0,  year?:number): void {
    this.getPeople(query, page, year)
      .pipe()
      .subscribe((result: any) => {              
          this.searchDataFetched.next(result);
      });
  }
  
  private createSearchRoute(query:string = 'back', page:number = 0, year?:number): string{
    const qryString = `?query=${query}&page=${page+1}${year ? "&year=" + year : ''}`;
    return qryString;
  }

  // #Observables
  // Here the observable for getting movies is defined, however it will not pass any data until it is
  // subscribed to as shown above.
  private getMovies(query:string = 'back', page:number = 0, year?:number): Observable<any> {
    return this._http.get<any>(`https://api.themoviedb.org/3/search/movie${this.createSearchRoute(query, page, year)}`);
  }

  private getPeople(query:string = 'back', page:number = 0, year?:number): Observable<any> {
    return this._http.get<any>(`https://api.themoviedb.org/3/search/person${this.createSearchRoute(query, page)}`);
  }
}
