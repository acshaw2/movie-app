import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardModule } from './dashboard/dashboard.module';
import { AuthModule } from './auth/auth.module';
import { SearchModule } from './search/search.module';

import { DashboardComponent } from './dashboard/components/dashboard/dashboard.component';
import { LoginComponent } from './auth/components/login/login.component';
import { MovieSearchComponent } from './search/components/movie-search/movie-search.component';
import { PeopleSearchComponent } from './search/components/people-search/people-search.component';
import { MovieDetailsComponent } from './details/components/movie-details/movie-details.component';

import { AuthGuardService } from './auth/services/auth-guard.service';
import { DashboardGuardService } from './dashboard/services/dashboard-guard.service';
import { MovieDetailsModule } from './details/movie-details.module';
import { PeopleDetailsComponent } from './details/components/people-details/people-details.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [AuthGuardService] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [DashboardGuardService]},
  { path: 'moviesearch', component: MovieSearchComponent },
  { path: 'peoplesearch', component: PeopleSearchComponent },
  { path: 'details/movie/:movieId', component: MovieDetailsComponent},
  { path: 'details/person/:personId', component: PeopleDetailsComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), AuthModule, SearchModule, MovieDetailsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
