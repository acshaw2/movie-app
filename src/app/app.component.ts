// #Closure
// I am not using closure in this app. Closure is one way to create something
// similar to a class with private methods in js. An example is below:
//
// var counter = (function() {
//   var privateCounter = 0;
//   function changeBy(val) {
//     privateCounter += val;
//   }
//
//   return {
//     increment: function() {
//       changeBy(1);
//     },
//
//     decrement: function() {
//       changeBy(-1);
//     },
//
//     value: function() {
//       return privateCounter;
//     }
//   };
// })();
// 
// This function allows calls like counter.increment() and counter.decrement()

// #Map
// Maps are used to apply a function to every value over an array. Maps were not used in this app
// let arr = [0,1,2,3];
// let mappedArr = arr.map(val => val + 1);
// the mappedArr would contain [1,2,3,4] as each value in the array would have 1 added to it.

// #Reducer
// Reducers facilitate the application of some operation across each value of an array such that a single
// output is produced. No reducers are used in this app.
// let arr = [0,1,2,3];
// let sumCallback = (sum, val) => sum + val;
// let result = arr.reduce(sumCallback);
// result will equal 6 as each value will get added to the next

// #Destructuring
// Destructuring lets arrays be pulled appart for easy assigning as shown in the example below. I always
// thought this was more common in react where state is immutable. This app does not use destructuring
// const [a, ...b] = [1, 2, 3];
// a would equal 1 and b would equal [2,3]

// #Spread
// Spread operator lets a an array of values be distributed. This app does not use spread operators
// let arr = [0,1,2];
// function myThreeInputFxn(x, y, z){ //sums stuff };
// let sum = myThreeInputFxn(...arr);

// #Promises
// These are how http requests are made. They proxy as an answer until the request can come back. This
// app does not use promises as observables are generally more useful.

// #Type definitions
// What types? heres a throwback for you instead, I am sure it's been a while: https://www.destroyallsoftware.com/talks/wat

import { Component } from '@angular/core';
import { AuthService } from './auth/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'movie-app';
  public showLogoutButton = false;

  constructor(private _authService: AuthService, private _router: Router){
    this._authService.isAuthenticatedChange.subscribe(value => {this.showLogoutButton = value});
  }

  public Logout():void {
    this._authService.toggleAuthenticated();   
    this._router.navigateByUrl('/login');
  }
}
