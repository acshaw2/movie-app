import { Directive, ElementRef, HostListener } from '@angular/core';

// #Directive
// This component is a sample directive that shows/hides some element 
// on mouseover events
@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  constructor(private el: ElementRef) { }

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight('');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight('pre');
  }

  private highlight(setting: string) {
    this.el.nativeElement.style.whiteSpace = setting;
  }
}