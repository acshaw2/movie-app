import {Component} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieCollectionService } from 'src/app/dashboard/services/movie-collection.service';
import { PeopleDetailsService } from '../../services/people-details.service';

@Component({
    selector: 'people-details',
    templateUrl: './people-details.component.html',
    styleUrls: ['./people-details.component.css']
})

export class PeopleDetailsComponent { 

    public cast: any[] = [];
    public crew: any[] = [];
    public person: IPerson = { id: 1,
      also_known_as: [],
      name: '',
      birthday: '',
      homepage: '',
      biography: '',
      profile_path: '',
      crew: [],
      cast: [],};
    public loading: boolean = true;

    constructor(private _route:ActivatedRoute, private _peopleDetailsService: PeopleDetailsService, private _collectionService: MovieCollectionService){ 
        let routeSub = this._route.params.subscribe(params => {
            if(params['personId']){                
                this._peopleDetailsService.GetPersonDetails(params['personId'])
                .pipe()
                .subscribe((result: IPerson) => {  
                    this.person = result;
                    this._peopleDetailsService.GetCredits(result.id)
                      .pipe()
                      .subscribe((credits: ICredits) =>{
                        this.person.crew = credits.crew;
                        this.person.cast = credits.cast;
                        this.loading = false;
                      });   
                });
            }
          });
    }    

    public AddToFavorites(person: any){
        this._collectionService.AddPerson(person);
    }
}

// #Interface
// Typescripts answer to typed objects as opposed to
// js's willy nilly loosely typed nonsense (that I have abused
// heavily in this app)
export interface IPerson {
    id: number;
    also_known_as: string[];
    name: string;
    birthday: string;
    homepage: string;
    biography: string;
    profile_path: string;
    crew: ICrew[];
    cast: ICast[];
  }

  export interface ICrew {
    title: string;
    poster_path: string;
    department: string;
  }

  export interface ICast {
    title: string;
    poster_path: string;
    character: string
  }

  export interface ICredits {
      crew: ICrew[];
      cast: ICast[];
  }