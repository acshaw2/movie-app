import {Component} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieCollectionService } from 'src/app/dashboard/services/movie-collection.service';
import { MovieDetailsService } from '../../services/movie-details.service';

@Component({
    selector: 'movie-details',
    templateUrl: './movie-details.component.html',
    styleUrls: ['./movie-details.component.css']
})

export class MovieDetailsComponent { 

    public movie: any;

    constructor(private _route:ActivatedRoute, private _movieDetailsService: MovieDetailsService, private _collectionService: MovieCollectionService){ 
        let routeSub = this._route.params.subscribe(params => {
            if(params['movieId']){                
                this._movieDetailsService.GetMovieDetails(params['movieId'])
                .pipe()
                .subscribe((result: any) => {        
                    this.movie = result;
                    this.movie.poster_path = `https://image.tmdb.org/t/p/w300${this.movie.poster_path}`
                    this.movie.backdrop_path = `https://image.tmdb.org/t/p/w300${this.movie.backdrop_path}`
                });
            }
          });
    }    

    public AddToFavorites(movie: any){
        this._collectionService.AddMovie(movie);
    }
}