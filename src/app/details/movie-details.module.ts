import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MovieDetailsComponent } from './components/movie-details/movie-details.component';
import { MovieDetailsService } from './services/movie-details.service';
import { HighlightDirective } from './directives/highlight.directive';
import { MatButton, MatButtonModule } from '@angular/material/button';
import { PeopleDetailsComponent } from './components/people-details/people-details.component';
import { PeopleDetailsService } from './services/people-details.service';



@NgModule({
  declarations: [
    MovieDetailsComponent,
    PeopleDetailsComponent,
    HighlightDirective
  ],
  exports: [
    MovieDetailsComponent,
    PeopleDetailsComponent,
    HighlightDirective
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatButtonModule
  ],
  providers: [
    MovieDetailsService,
    PeopleDetailsService
  ]
})
export class MovieDetailsModule { }