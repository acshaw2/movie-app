import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; 

@Injectable({
  providedIn: 'root'
})
export class MovieDetailsService {

  constructor(private _http: HttpClient) { }

  public GetMovieDetails(id: number): Observable<any> {
    return this._http.get<any>(`https://api.themoviedb.org/3/movie/${id}?language=en`);    
  }
}
