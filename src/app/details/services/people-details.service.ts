import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; 
import { ICredits, IPerson } from '../components/people-details/people-details.component';

@Injectable({
  providedIn: 'root'
})
export class PeopleDetailsService {

  constructor(private _http: HttpClient) { }

  public GetPersonDetails(id: number): Observable<any> {
    return this._http.get<IPerson>(`https://api.themoviedb.org/3/person/${id}?language=en`);    
  }  

  public GetCredits(id:number): Observable<ICredits> {
    return this._http.get<any>(`https://api.themoviedb.org/3/person/${id}/movie_credits?language=en`);
  }
}
